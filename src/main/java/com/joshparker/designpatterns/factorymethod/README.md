*********Factory Method Pattern*********


**What is it?**
- It's one of the most used design patterns in Java

- A creational design pattern which solves the problem of creating product objects without specifying their concrete classes.

- A way we create object without exposing the creation logic to the client and refer to newly created object using a common interface.

- It defines a method, which should be used for creating objects instead of direct constructor call (new operator). Subclasses can override this method to change the class of objects that will be created. It’s like using a virtual constructor. Using the new operator is considered harmful.

**Why we use it?**
- It solves the problem of creating product objects without specifying their concrete classes.

- It gives you a lot more flexibility when it comes time to change the application (i.e. you can create new implementations without changing the dependent code)

- It makes your code more testable as you can mock interfaces

- Follows the SOLID principles more closely. In particular, the interface segregation and dependency inversion principles.

- Factory Method Pattern allows the sub-classes to choose the type of objects to create.

- It promotes the loose-coupling by eliminating the need to bind application-specific classes into the code. That means the code interacts solely with the resultant interface or abstract class, so that it will work with any classes that implement that interface or that extends that abstract class.

**When should we use it?**
- When you need to provide a high level of flexibility for your code.

- When a class doesn't know what sub-classes will be required to create

- When a class wants its sub-classes to specify the objects to be created.

- When the parent classes choose the creation of objects to its sub-classes.

**Example...**

<img src="/uploads/82eb0384545dd014e4801093773f1fb4/factory-method-pattern.jpg" alt="alt text" width="80%" height="80%">



