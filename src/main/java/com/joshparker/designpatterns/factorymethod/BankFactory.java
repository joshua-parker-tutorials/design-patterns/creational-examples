package com.joshparker.designpatterns.factorymethod;

import com.joshparker.designpatterns.factorymethod.models.BOYDS;
import com.joshparker.designpatterns.factorymethod.models.Bank;
import com.joshparker.designpatterns.factorymethod.models.HCO;
import com.joshparker.designpatterns.factorymethod.models.PPO;

public class BankFactory {

    public Bank getBank(String bank) {
        if (bank == null) {
            return null;
        }

        else if (bank.equalsIgnoreCase("PPO")) {
            return new PPO();
        }

        else if (bank.equalsIgnoreCase("HCO")) {
            return new HCO();
        }

        else if (bank.equalsIgnoreCase("BOYDS")) {
            return new BOYDS();
        }

        return null;
    }
}
