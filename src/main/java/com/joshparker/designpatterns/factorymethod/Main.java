package com.joshparker.designpatterns.factorymethod;

import com.joshparker.designpatterns.factorymethod.models.Bank;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        BankFactory bankFactory = new BankFactory();

        Bank ppo = bankFactory.getBank("PPO");
        System.out.println("\n=== PPOs details ===");
        System.out.println("Interest rate: " + ppo.gatInterestRate());
        System.out.println("Customer satisfactory rate: " + ppo.getCustomerSatisfactoryRate());

        Bank hco = bankFactory.getBank("HCO");
        System.out.println("\n=== HCOs details ===");
        System.out.println("Interest rate: " + hco.gatInterestRate());
        System.out.println("Customer satisfactory rate: " + hco.getCustomerSatisfactoryRate());

        Bank boyds = bankFactory.getBank("BOYDS");
        System.out.println("\n=== BOYDS details ===");
        System.out.println("Interest rate: " + boyds.gatInterestRate());
        System.out.println("Customer satisfactory rate: " + boyds.getCustomerSatisfactoryRate());
    }
}
