package com.joshparker.designpatterns.factorymethod.models;

public class HCO implements Bank {
    public String gatInterestRate() {
        return "9.5%";
    }

    public String getCustomerSatisfactoryRate() {
        return "78%";
    }
}
