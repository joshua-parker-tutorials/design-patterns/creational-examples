package com.joshparker.designpatterns.factorymethod.models;

public class PPO implements Bank {

    public String gatInterestRate() {
        return "12.5%";
    }

    public String getCustomerSatisfactoryRate() {
        return "80%";
    }

}
