package com.joshparker.designpatterns.factorymethod.models;

public interface Bank {
    String gatInterestRate();
    String getCustomerSatisfactoryRate();
}
