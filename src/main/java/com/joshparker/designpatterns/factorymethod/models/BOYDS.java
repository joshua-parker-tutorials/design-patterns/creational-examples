package com.joshparker.designpatterns.factorymethod.models;

public class BOYDS implements Bank {
    public String gatInterestRate() {
        return "6%";
    }

    public String getCustomerSatisfactoryRate() {
        return "97%";
    }
}
