package com.joshparker.designpatterns.abstractfactory;

import com.joshparker.designpatterns.abstractfactory.factory.AbstractFactory;
import com.joshparker.designpatterns.abstractfactory.factory.FactoryProvider;
import com.joshparker.designpatterns.abstractfactory.model.Vehicle;

public class Main {
    public static void main(String[] args) {
        AbstractFactory landVehicleFactory1 = FactoryProvider.getVehicleFactory("LAND VEHICLE");
        Vehicle car = landVehicleFactory1.getVehicle("CAR");
        car.accelerate(20);

        AbstractFactory landVehicleFactor2 = FactoryProvider.getVehicleFactory("LAND VEHICLE");
        Vehicle van = landVehicleFactor2.getVehicle("VAN");
        van.accelerate(40);

        AbstractFactory seaVehicleFactor1 = FactoryProvider.getVehicleFactory("SEA VEHICLE");
        Vehicle ship = seaVehicleFactor1.getVehicle("SHIP");
        ship.accelerate(100);

        AbstractFactory seaVehicleFactor2 = FactoryProvider.getVehicleFactory("SEA VEHICLE");
        Vehicle submarine = seaVehicleFactor2.getVehicle("SUBMARINE");
        submarine.accelerate(200);
    }
}
