package com.joshparker.designpatterns.abstractfactory.factory;

import com.joshparker.designpatterns.abstractfactory.model.Car;
import com.joshparker.designpatterns.abstractfactory.model.Van;
import com.joshparker.designpatterns.abstractfactory.model.Vehicle;

public class LandVehicleFactory extends AbstractFactory {

    @Override
    public Vehicle getVehicle (String vehicle) {
        switch (vehicle.toUpperCase()) {
            case "CAR":
                return new Car();
            case "VAN":
                return new Van();
        }
        return null;
    }
}
