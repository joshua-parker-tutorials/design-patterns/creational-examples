package com.joshparker.designpatterns.abstractfactory.factory;

import com.joshparker.designpatterns.abstractfactory.model.Vehicle;

public abstract class AbstractFactory {
    public abstract Vehicle getVehicle(String vehicleType);
}
