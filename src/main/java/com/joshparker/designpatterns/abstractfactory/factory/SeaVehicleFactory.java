package com.joshparker.designpatterns.abstractfactory.factory;

import com.joshparker.designpatterns.abstractfactory.model.Ship;
import com.joshparker.designpatterns.abstractfactory.model.Submarine;
import com.joshparker.designpatterns.abstractfactory.model.Vehicle;

public class SeaVehicleFactory extends AbstractFactory {

    @Override
    public Vehicle getVehicle(String vehicle) {
        switch (vehicle.toUpperCase()) {
            case "SHIP":
                return new Ship();
            case "SUBMARINE":
                return new Submarine();
        }
        return null;
    }
}
