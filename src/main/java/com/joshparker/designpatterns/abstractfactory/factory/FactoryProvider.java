package com.joshparker.designpatterns.abstractfactory.factory;

public final class FactoryProvider {

    private FactoryProvider(){}

    public static AbstractFactory getVehicleFactory(String vehicleType) {
        switch (vehicleType.toUpperCase()) {
            case "LAND VEHICLE":
                return new LandVehicleFactory();
            case "SEA VEHICLE":
                return new SeaVehicleFactory();
        }
        return null;
    }
}
