package com.joshparker.designpatterns.abstractfactory.model;

public class Submarine implements Vehicle {
    @Override
    public void accelerate(int speed) {
        System.out.println("Submarine accelerating to " + speed + " KNOTS");
    }
}
