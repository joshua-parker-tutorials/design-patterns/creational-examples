package com.joshparker.designpatterns.abstractfactory.model;

public class Ship implements Vehicle {
    @Override
    public void accelerate(int speed) {
        System.out.println("Ship accelerating to " + speed + " KNOTS");
    }
}
