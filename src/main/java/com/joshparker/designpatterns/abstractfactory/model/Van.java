package com.joshparker.designpatterns.abstractfactory.model;

public class Van implements Vehicle {
    @Override
    public void accelerate(int speed) {
        System.out.println("Van accelerating to " + speed + " MPH");
    }
}
