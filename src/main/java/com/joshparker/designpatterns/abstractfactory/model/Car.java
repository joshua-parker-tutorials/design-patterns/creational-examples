package com.joshparker.designpatterns.abstractfactory.model;

public class Car implements Vehicle {
    @Override
    public void accelerate(int speed) {
        System.out.println("Car accelerating to " + speed + " MPH");
    }
}
