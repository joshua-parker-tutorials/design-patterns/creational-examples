package com.joshparker.designpatterns.abstractfactory.model;

public interface Vehicle {
    void accelerate(int speed);
}
